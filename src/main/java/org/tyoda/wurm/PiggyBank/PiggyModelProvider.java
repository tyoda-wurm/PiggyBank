/*
PiggyBank mod for Wurm Unlimited
Copyright (C) 2024 Tyoda

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package org.tyoda.wurm.PiggyBank;

import com.wurmonline.server.items.Item;
import org.gotti.wurmunlimited.modsupport.items.ModelNameProvider;

public class PiggyModelProvider implements ModelNameProvider {
    @Override
    public String getModelName(Item item) {
        StringBuilder sb = new StringBuilder(item.getTemplate().getModelName());

        //if (item.getDamage() >= 50f)
        //    sb.append("decayed.");

        return sb.toString();
    }
}