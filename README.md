# PiggyBank
Are you tired of managing coins in your inventory and you are also scared of the bank? Piggy banks might just be the thing for you!

This mod adds an, allegedly, very cute piggy bank to the game, in which you can deposit your hard earned coins for later retrieval...by force...

![piggy banks on a table](piggies.png)


# Dependencies
For this mod to work you need to have <a href="https://github.com/ago1024/WurmServerModLauncher">ago's server modlauncher</a>,
and both the <a href="https://gitlab.com/tyoda-wurm/Iconzz">Iconzz</a> library mod 
and the <a href="https://gitlab.com/tyoda-wurm/CommonLibrary">CommonLibrary</a> mod installed

# Features
 - Create a clay piggy bank with your hands!
 - Fire the piggy bank!
 - Put money in the piggy bank!
 - Throw the piggy bank on something hard!

## On a serious note:
 - Coins can be deposited into the piggy bank after which the piggy bank's weight grows by the coin's weight
 - You have no idea how much money is in any given piggy bank, your only clue is how much it weighs.
 - Throw it at an assortment of hard items and tiles to retrieve the goodies!

### On a less serious note:

Don't forget to paint your piggy banks pink for the complete experience!


# Add to gradle

Since this mod might be used by others, such as my fork of [TreasureHunting](https://gitlab.com/tyoda-wurm/TreasureHunting), here is a way to use it in your mod:

To include PiggyBank in your gradle project, add it to the repositories in build.gradle

```gradle
repositories {
    // other repositories
    maven { url "https://gitlab.com/api/v4/groups/83701471/-/packages/maven" }
}
```

and then include it as a dependency as

```gradle
dependencies {
    // other dependencies
    compileOnly 'org.tyoda.wurm.PiggyBank:PiggyBank:<VERSION>'
}
```

or

```gradle
dependencies {
    // other dependencies
    implementation 'org.tyoda.wurm.PiggyBank:PiggyBank:<VERSION>'
}
```

replacing `<VERSION>` with the current latest release version.

# Add to maven

To include Iconzz in your maven project, add it to the repositories in pom.xml


```xml
<repository>
    <id>iconzz-gitlab</id>
    <name>Iconzz GitLab repository</name>
    <url>https://gitlab.com/api/v4/groups/83701471/-/packages/maven</url>
</repository>
```

Then add the depndency

```xml
<dependency>
  <groupId>org.tyoda.wurm.PiggyBank</groupId>
  <artifactId>PiggyBank</artifactId>
  <version><VERSION></version>
</dependency>
```

replacing `<VERSION>` with the current latest release version.

(full disclosure: I don't know anything about maven, hopefully nobody tries to use this)


# Credits
- The models and icons were all made by the one and only Starstorm
